#!/bin/bash
set -e
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

cd ..

repos=(
utilities
customer-a
pi
merchant-api
bank-service
mobile-app
token-service
manager-api
account-manager
payment-manager
system
)

for REPO in ${repos[*]};
    do
         if [ -f "${REPO}/generated/swagger-ui/${REPO}.yaml" ]; then
            cp ${REPO}/generated/swagger-ui/${REPO}.yaml $SCRIPT_DIR/swagger-ui/
        fi


        if [[ -d "${REPO}/target/" ]]; then
            filename=$(find ${REPO}/target/ -type f -name "*.puml")
            if [[ ! -z $filename ]]; then
            java -jar $SCRIPT_DIR/plantuml-1.2022.0.jar -o $SCRIPT_DIR/class-diagram $filename 

            fi
        fi
    done

cd $SCRIPT_DIR