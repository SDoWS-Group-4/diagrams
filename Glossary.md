# Glossary 
## DTU Pay:
1. DTU Pay is a company that offers a mobile payment option for shop owners (i.e. merchants) and customers. 
2. Only DTU Pay knows to which customer a token belongs.
3. DTU Pay holds a record of all payments done via DTU Pay for each customer plus the tokens used in these payments.
4. Also note that the bank accounts for customer and merchant are not managed by DTU Pay.

## Customer and merchant
1. Both, customer and merchant, already have a bank account with the bank (the bank is
supplied by me). To use the service, 
2. both, customers and merchants have to be registered with DTU Pay with their bank account id’s / account numbers.
3. To take part in the mobile payment process, both, the customer and the merchant, have to have mobile devices.
In the payment process, 

4. When the customer and the merchant register to DTU Pay, they already have a bank account. 
5. Both, merchant and customer supply their respective bank acount ids when registering with DTU Pay.
6. Note that, in principle, a person can have several bank accounts. So it is important that the customer and
the merchant tell DTU Pay which bank account to use.

## Merchant
1. Both, customer and merchant, already have a bank account with the bank (the bank is
supplied by me). To use the service, 
2. both, customers and merchants have to be registered with DTU Pay with their bank account id’s / account numbers.
3. To take part in the mobile payment process, both, the customer and the merchant, have to have mobile devices.
In the payment process,
4. The merchant "just knows" the token that the customer uses for payment.


## Customer 
1. the customer can present a unique token he has previously received from
DTU Pay to the merchant, e.g. using RFID technology, as a proof the customer has agreed to the payment (cf. Fig. 1).
2. For the payment the customer has to have at least one unused token in his posession.
3. The customer can request 1 to 5 tokens if he either has spent all tokens (or it is the first time
he requests tokens) or has only one unused token left.
4. Overall, a customer can only have at most 6 unused tokens.
5. If the customer has more than 1 unused token and he requests again a set of tokens, his request will be denied.

## Token 
1. A token consists of a unique number/string (what is a good choice?).
2.  The tokens in the system should be unique and it should not possible to guess an unused token. 
3. Furthermore, for privacy reasons, the token cannot contain any information about the customer to whom the token belongs. 
4. A token can only be used for one payment.
5. Note that the functionality of the terminal that reads the RFID token is not part of the project.


# The features that should be implemented are:
* Pay a merchant, which includes transferring money from the account that the customer has in the bank to the account of the merchant in the bank. 
* The account ids for the customer and merchants are provided when customer and merchant register with DTU Pay. DTU Pay does not create bank accounts.
* Thus it is important to mention which bank account DTU Pay should use when registering a customer/merchant.
* Customers can obtain unique tokens (i.e. Token Management) according to the rules outlined above.
* Managing customer and merchant accounts with DTU Pay (i.e. Account Management)
* The merchant and customer register themselves via their DTU Pay apps.

* A reporting function for the manager or DTU Pay to see all payments and a summary of all the money being transfered. 
  * For simplicity it is assumed that there is only one manager. That means, there is no need for managing manager accounts.

* A reporting function, that generates for a customer the list of his payments done with DTU Pay
(amount of money transferred, with which merchant, and token used). 
* DTU Pay has to self keep track of the payment transactions. 
* It is not possible to use the transactions stored in the bank.
* These transactions may contain transactions not done through DTU Pay, 
* DTU Pay should not see those transactions.

* A reporting function, that generates for a merchant the list of the payments with DTU Pay he
was involved with (amount of money transferred and token used). 
* Note that the merchant should not know who the customer was. 
* Thus the customer (only the token) should not appear in the list of payments that the merchant sees.

# Good grade Project notes
7. For the planning of your work, think about the resource triangle: time, functionality, and quality.
8. The time for the project is fixed.
9.  Quality should be fixed too. Functionality is therefor variable.
10. Grading will value quality over quantity. Quality here means, that your design and implementation applies the concepts taught in the course, e.g. using 
    1.   domain-driven design, 
    2.   hexagonal architecture,
    3.   S.O.L.I.D principles, 
    4.   endpoint design according to the REST architecture principles, use of messaging,
etc. 
11. When you choose a functionality to implement next, you should think about how good it will
show that you have achieved specific learning objectives of the course.
12. You should look
    1.   first at the successful payment process, 
    2.   then at token management, 
    3.   and then at reporting. 
13.  After that you should also look at possible fail scenarios, e.g. when payment should not succeed (e.g. merchant is unknown, the customers token is not known, or invalid, or already used,etc.