```plantuml
    skinparam monochrome true
    skinparam backgroundColor transparent
    skinparam classBackgroundColor transparent
    skinparam style strictuml
    skinparam SequenceGroupBodyBackGroundColor transparent

    mainframe Register Customer

    actor Customer as customer
    participant "Mobile-app" as app
    participant "Customer-api" as api
    participant "Account-manager" as manager
    
    activate customer
        customer -> app : Register
        activate app
            app -> api : POST customer/register
            activate api
                api -> manager : CustomerRegistrationRequested
                activate manager
                manager -> manager : Register \nCustomer
                group successfull
                manager -->> api : CustomerIdAssigned
                else Error
                manager -->> api : CustomerNotAssigned
                deactivate manager
                end
            api -->> app : Response
            deactivate api
            app -->> customer : Result
        deactivate app
```

```plantuml
    skinparam monochrome true
    skinparam backgroundColor transparent
    skinparam classBackgroundColor transparent
    skinparam style strictuml
    skinparam SequenceGroupBodyBackGroundColor transparent

    mainframe Register Merchant

    actor Merchant as merchant
    participant "Mobile-app" as app
    participant "Merchant-api" as api
    participant "Account-manager" as manager
    
    activate merchant
        merchant -> app : Register
        activate app
            app -> api : POST merchant/register
            activate api
                api -> manager : MerchantRegistrationRequested
                activate manager
                manager -> manager : Register \nMerchant
                group successfull
                manager -->> api : MerchantIdAssigned
                else Error
                manager -->> api : MerchantNotAssigned
                deactivate manager
                end
            api -->> app : Response
            deactivate api
            app -->> merchant : Result
        deactivate app
```