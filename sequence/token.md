```plantuml
    skinparam monochrome true
    skinparam backgroundColor transparent
    skinparam classBackgroundColor transparent
    skinparam style strictuml
    skinparam SequenceGroupBodyBackGroundColor transparent

    mainframe GenerateTokens Customer

    actor Customer as customer
    participant "Mobile-app" as app
    participant "Customer-api" as api
    participant "Token-manager" as manager
    
    activate customer
        customer -> app : getToken
        activate app
            app -> api : Get customer/token
            activate api
                api -> manager : GenerateTokenRequest
                activate manager
                manager -> manager : generateTokens
                group successfull
                manager -->> api : TokensGenerated
                else Error
                manager -->> api : TokensNotGenerated
                deactivate manager
                end
            api -->> app : Response
            deactivate api
            app -->> customer : Result
        deactivate app
```