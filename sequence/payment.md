```plantuml
    skinparam monochrome true
    skinparam backgroundColor transparent
    skinparam classBackgroundColor transparent
    skinparam style strictuml
    skinparam SequenceGroupBodyBackGroundColor transparent

    mainframe Payment

    actor Merchant as merchant
    participant "Mobile-app" as app
    participant "Customer-api" as customerApi
    participant "Merchant-api" as merchantApi
    participant "Payment-manager" as manager
    participant "Bank-service" as bank
    
    activate merchant
        merchant -> app : pay
        activate app
            app -> merchantApi : POST merchant/pay
            activate merchantApi
                merchantApi -> customerApi : request token
                activate customerApi
                    customerApi -->> merchantApi : token
                deactivate customerApi
                    group successfull:
                        merchantApi -> manager : PaymentRequested
                        activate manager
                            manager -> bank : TransferMoneyRequested
                            activate bank
                                bank -> bank : transferMoneyFromTo
                                bank -->> manager : MoneyTransfered
                            deactivate bank
                            manager -->> merchantApi : PaymentSuccessful
                        deactivate manager
                    else Error
                        merchantApi -> manager : PaymentRequested
                        activate manager
                            manager -> bank : TransferMoneyRequested
                            activate bank
                                bank -> bank : transferMoneyFromTo
                                bank -->> manager : MoneyNotTransfered
                            deactivate bank
                            manager -->> merchantApi : PaymentNotSuccessful
                        deactivate manager
                    end
            merchantApi -->> app : Response
            deactivate merchantApi
            app -->> merchant : Result
        deactivate app
```