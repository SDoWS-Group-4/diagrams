```plantuml
    skinparam monochrome true
    skinparam backgroundColor transparent
    skinparam classBackgroundColor transparent
    skinparam style strictuml
    skinparam SequenceGroupBodyBackGroundColor transparent

    mainframe Deregister Customer

    actor Customer as customer
    participant "Mobile-app" as app
    participant "Customer-api" as api
    participant "Account-manager" as manager
    
    activate customer
        customer -> app : deregister
        activate app
            app -> api : POST customer/deregister
            activate api
                api -> manager : CustomerDeregistrationRequested
                activate manager
                manager -> manager : deregister \nCustomer
                group successfull
                manager -->> api : CustomerDeregistered
                else Error
                manager -->> api : CustomerNotDeregistered
                deactivate manager
                end
            api -->> app : Response
            deactivate api
            app -->> customer : Result
        deactivate app
```

```plantuml
    skinparam monochrome true
    skinparam backgroundColor transparent
    skinparam classBackgroundColor transparent
    skinparam style strictuml
    skinparam SequenceGroupBodyBackGroundColor transparent

    mainframe Deregister Merchant

    actor Merchant as merchant
    participant "Mobile-app" as app
    participant "Merchant-api" as api
    participant "Account-manager" as manager
    
    activate merchant
        merchant -> app : deregister
        activate app
            app -> api : POST merchant/deregister
            activate api
                api -> manager : MerchantDeregistrationRequested
                activate manager
                manager -> manager : deregister \nMerchant
                group successfull
                manager -->> api : MerchantDeregistered
                else Error
                manager -->> api : MerchantNotDeregistered
                deactivate manager
                end
            api -->> app : Response
            deactivate api
            app -->> merchant : Result
        deactivate app
```